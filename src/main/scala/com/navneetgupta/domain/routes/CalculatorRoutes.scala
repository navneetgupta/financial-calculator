package com.navneetgupta.domain.routes

import akka.actor.ActorRef
import com.navneetgupta.infrastructure.routes.BaseRouteDefination
import com.navneetgupta.domain.jsonprotocols.CalculatorJsonProtocol
import akka.stream.Materializer
import akka.actor.ActorSystem
import scala.concurrent.ExecutionContext
import akka.http.scaladsl.server.Route
import com.navneetgupta.domain.model._

class CalculatorRoutes(calculatorManagerRef: ActorRef) extends BaseRouteDefination with CalculatorJsonProtocol {

  import akka.pattern._
  import akka.http.scaladsl.server.Directives._

  override def routes(implicit system: ActorSystem, ec: ExecutionContext, mater: Materializer): Route = {
    pathPrefix("calculate") {
      pathEndOrSingleSlash {
        post {
          entity(as[InputModel]) { input =>
            serviceAndComplete[ResponseModel](input, calculatorManagerRef)
          }
        }
      }
    }
  }
}
