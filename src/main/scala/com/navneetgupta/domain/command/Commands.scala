package com.navneetgupta.domain.command

import com.navneetgupta.domain.model.CalculatorModel

case class CalculateRate(calculatorModel: CalculatorModel)
