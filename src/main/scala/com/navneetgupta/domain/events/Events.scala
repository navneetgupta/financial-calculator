package com.navneetgupta.domain.events

case class APRCalculated(apr: Double) {
  def getRoundedAPR = {
    Math.round(Math.ceil(apr * 10000) / 10.0) / 10.0
  }
}

case class IRRCalculated(irr: Double)
