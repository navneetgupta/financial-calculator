package com.navneetgupta.domain.main

import com.navneetgupta.infrastructure.boot.Bootstrap
import akka.actor.ActorSystem
import com.navneetgupta.domain.actors.CalculatorManager
import com.navneetgupta.domain.routes.CalculatorRoutes

class CalculatorBootstrap extends Bootstrap {
  override def bootstrap(system: ActorSystem) = {
    import system.dispatcher
    val calculatorManager = system.actorOf(CalculatorManager.props, CalculatorManager.Name)
    List(new CalculatorRoutes(calculatorManager))
  }
}
