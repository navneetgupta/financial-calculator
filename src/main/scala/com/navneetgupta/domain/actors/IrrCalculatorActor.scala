package com.navneetgupta.domain.actors

import scala.annotation.tailrec
import akka.actor.Props
import com.navneetgupta.infrastructure.actors.BaseActor
import com.navneetgupta.domain.command.CalculateRate
import com.navneetgupta.domain.logic.CalculatorAlgebra
import com.navneetgupta.domain.events.IRRCalculated
import com.navneetgupta.domain.model._

class IrrCalculatorActor extends BaseActor {
  import CalculatorModel._
  import AlgebraInstances._
  override def receive = {
    case CalculateRate(calculatorModel) =>
      log.info("Calculating IRR Rate")
      //      val spa = new CalculatorAlgebra(x => calculatorModel.presentValue(x, PayBack.calculateSum))
      //      sender ! IRRCalculated(spa.getUsingApproximateMethod(0.0))
      val spa = new CalculatorAlgebra(calculatorModel.calculate, calculatorModel.calculateDerivative)
      //Taking First guess for root as 0.0
      sender ! IRRCalculated(spa.newtonsMethod(0.0))
    case _ =>
  }
}

object IrrCalculatorActor {
  def props = Props[IrrCalculatorActor]
}
