package com.navneetgupta.domain.actors

import akka.actor.Props
import com.navneetgupta.infrastructure.actors.BaseActor
import scala.concurrent.Future
import com.navneetgupta.domain.logic.CalculatorAlgebra
import com.navneetgupta.domain.model.PayBack
import akka.stream.scaladsl.RunnableGraph
import akka.stream.scaladsl.GraphDSL
import akka.NotUsed
import akka.stream.scaladsl._
import akka.stream.ClosedShape
import com.navneetgupta.domain.model.CalculatorModel
import com.navneetgupta.domain.command.CalculateRate
import com.navneetgupta.domain.model.ResponseModel
import cats._
import cats.implicits._
import com.navneetgupta.domain.events.APRCalculated
import com.navneetgupta.domain.events.IRRCalculated
import com.navneetgupta.domain.model.InputModel

object CalculatorManager {
  def props = Props[CalculatorManager]
  val Name = "calculator-manager"
}
class CalculatorManager extends BaseActor {
  import context.dispatcher
  import akka.pattern.ask

  val aprCalculator = context.actorOf(AprCalculatorActor.props, "aprCalculator")
  val irrCalculator = context.actorOf(IrrCalculatorActor.props, "irrCalculator")

  override def receive = {
    case cac @ InputModel(principal, uff, schedule) =>
      val aprModel = InputModel.getAPRCalculatorModel(cac)
      val apr = (aprCalculator ? CalculateRate(aprModel)).mapTo[APRCalculated]
      val irr = (irrCalculator ? CalculateRate(aprModel)).mapTo[IRRCalculated]

      // Uses cats Function Library to Combine multiple futures into Single Future
      val combineFuture = Future { (a: APRCalculated, b: IRRCalculated) => ResponseModel(a.getRoundedAPR, b.irr) }

      pipeResponse(Apply[Future].ap2(combineFuture)(apr, irr))
  }
}
