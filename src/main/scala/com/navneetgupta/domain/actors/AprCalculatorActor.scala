package com.navneetgupta.domain.actors

import scala.annotation.tailrec
import com.navneetgupta.domain.model.PayBack
import com.navneetgupta.infrastructure.actors.BaseActor
import com.navneetgupta.domain.logic.CalculatorAlgebra
import com.navneetgupta.domain.command.CalculateRate
import akka.actor.Props
import com.navneetgupta.domain.events.APRCalculated
import com.navneetgupta.domain.model._

class AprCalculatorActor extends BaseActor {
  import CalculatorModel._
  import AlgebraInstances._
  override def receive = {
    case CalculateRate(calculatorModel) =>

      log.info("Calculating APR Rate")
      // Modifying Period for appropraite calculation. HEre we are using months.
      val aprModel = calculatorModel.copy(
        installments = calculatorModel.installments.map(a => a.copy(period = a.period / 12)))

      //      val spa = new CalculatorAlgebra(x => aprModel.presentValue(x, PayBack.calculateSum))
      //      sender ! APRCalculated(spa.getUsingApproximateMethod(0.0))

      val spa = new CalculatorAlgebra(aprModel.calculate, aprModel.calculateDerivative)
      //Taking First guess for root as 0.0
      sender ! APRCalculated(spa.newtonsMethod(0.0))
    case _ =>
  }
}

object AprCalculatorActor {
  def props = Props[AprCalculatorActor]
}
