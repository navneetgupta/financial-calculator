package com.navneetgupta.domain.model

import java.util.Date

case class Schedules(
  id: Int,
  date: Date,
  principal: Double,
  interestFee: Double)

case class Value(value: Double = 0.0)

case class InputModel(
    principal: Double,
    upfrontFee: Option[Value] = None,
    schedule: List[Schedules]) {
  require(principal > 0, "principal must be Greater than zero")
  require(schedule.size > 0, "schedule must have atleast one element")
}

object InputModel {
  def getAPRCalculatorModel(input: InputModel): CalculatorModel = {
    // Assuming StartDate is One Month before First
    val advance =
      if (input.upfrontFee.isDefined) input.principal - input.upfrontFee.get.value
      else input.principal
    //Sorting Schedules if Schedules is not sorted
    // Assumption: Payment Period is monthly which will start after first month of advacne
    val sortedSchedules = input.schedule.sortBy(_.date)
    val paybacks = sortedSchedules.foldLeft((Nil: List[PayBack], 0))((a, b) => (PayBack(b.principal + b.interestFee, a._2 + 1) :: a._1, a._2 + 1))._1
    CalculatorModel(advance, paybacks)
  }
}

case class ResponseModel(
  apr: Double,
  irr: Double)

object ResponseModel extends Function2[Double, Double, ResponseModel]
