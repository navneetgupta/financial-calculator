package com.navneetgupta.domain.model

case class PayBack(amount: Double, period: Double)

object PayBack {
  def calculateSum(apr: Double, payback: PayBack): Double = {
    val divisor = Math.pow(1 + apr, payback.period)
    payback.amount / divisor
  }
}
