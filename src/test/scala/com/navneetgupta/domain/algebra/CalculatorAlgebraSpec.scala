package com.navneetgupta.domain.algebra

import org.scalatest.FlatSpec
import com.navneetgupta.domain.logic.CalculatorAlgebra
import org.scalatest.FunSuite

class CalculatorAlgebraSpec extends FunSuite {
  test("CalculatorAlgebra Class for Newton-RapSon Method should calculate appropriate square root for 2") {
    val f: Double => Double = x => Math.pow(x, 2) - 2
    val df: Double => Double = x => 2 * Math.pow(x, 1)
    val sqrtOf2 = new CalculatorAlgebra(f, df)
    assert((Math.round(sqrtOf2.newtonsMethod(2.0) * 1000) / 1000.0) === 1.414)
  }

  test("CalculatorAlgebra Class for Newton-RapSon Method should calculate appropriate cube root for 2") {
    val f: Double => Double = x => Math.pow(x, 3) - 2
    val df: Double => Double = x => 3 * Math.pow(x, 2)
    val sqrtOf2 = new CalculatorAlgebra(f, df)
    assert((Math.round(sqrtOf2.newtonsMethod(2.0) * 1000) / 1000.0) === 1.26)
  }

  test("CalculatorAlgebra Class for Newton-RapSon Method should calculate appropriate root for given function") {
    val f: Double => Double = x => Math.pow(x, 5.0) - 2.0 * x
    val df: Double => Double = x => 5 * Math.pow(x, 4) - 2.0
    val sqrtOf2 = new CalculatorAlgebra(f, df)
    assert((Math.round(sqrtOf2.newtonsMethod(2.0) * 1000) / 1000.0) === 1.189)
  }
}
