# Financial Calculator Assessment  
[![pipeline status](https://gitlab.com/Notanull/financial-calculator/badges/master/pipeline.svg)](https://gitlab.com/Notanull/financial-calculator/commits/master)

Assumptions based on mail clarifications and assessment:

1. Advacne is made one month before first payback due.
2. Payback Installment period will be monthly.
3. Number of Installmetns, Interest and Principal of Paybacks may vary.
4. Upfrontfee Could be there or may not be there.
5. Ignoring upfrontCreditlineFee.
6. IRR is not rounded to any number of decimal places so digits after certain number of places after decimal might vary. due to precision

To run the project

```cd financial-calculator```  
```sbt run```


To Calculate IRR and APR using below Curl command    


```bash
curl -X POST \
  http://localhost:9000/calculator/calculate \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' \
  -H 'Postman-Token: 37f9d82e-85de-4e8d-90f0-af5d8752176a' \
  -d '{
  "principal": 51020400,
  "upfrontFee": {
    "value": 1020400
  },
  "schedule": [
    {
      "id": 1,
      "date": "2016-10-20",
      "principal": 3595000,
      "interestFee": 1530600
    },
    {
      "id": 2,
      "date": "2016-11-21",
      "principal": 3702800,
      "interestFee": 1422800
    },
    {
      "id": 3,
      "date": "2016-12-20",
      "principal": 3813900,
      "interestFee": 1311700
    },
    {
      "id": 4,
      "date": "2017-01-20",
      "principal": 3928300,
      "interestFee": 1197300
    },
    {
      "id": 5,
      "date": "2017-02-20",
      "principal": 4046200,
      "interestFee": 1079400
    },
    {
      "id": 6,
      "date": "2017-03-20",
      "principal": 4167600,
      "interestFee": 958000
    },
    {
      "id": 7,
      "date": "2017-04-20",
      "principal": 4292600,
      "interestFee": 833000
    },
    {
      "id": 8,
      "date": "2017-05-22",
      "principal": 4421400,
      "interestFee": 704200
    },
    {
      "id": 9,
      "date": "2017-06-20",
      "principal": 4554000,
      "interestFee": 571600
    },
    {
      "id": 10,
      "date": "2017-07-20",
      "principal": 4690600,
      "interestFee": 435000
    },
    {
      "id": 11,
      "date": "2017-08-21",
      "principal": 4831400,
      "interestFee": 294200
    },
    {
      "id": 12,
      "date": "2017-09-20",
      "principal": 4976600,
      "interestFee": 149300
    }
  ]
}'
```


